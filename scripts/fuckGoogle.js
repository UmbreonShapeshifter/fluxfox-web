/*
Copyright (C) 2023 Fluxfox

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
const isIOSChrome = navigator.userAgent.match("CriOS");

const isChromium = /Chromium/.test(UA) || /OPR/.test(UA) || /Chromium/.test(UA) || (/Edge/.test(UA) && windows.MSStream);

// Runs if the chromium is found
if (isChromium || isIOSChrome) {
	window.location.replace("/Pages/dontusechrome.html");
	console.log("Redirecting to blog post on why you shouldn't use Chrome, if your reading this. It's likely you already know why");
}
